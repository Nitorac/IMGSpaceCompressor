package imgspacecomp.algos;

import javax.swing.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public interface BaseAlgo {
    String getName();

    String[] getAvantages();

    String[] getInconvenients();

    String getErrorMessage(Exception e);

    ArrayList encode(double[][][] source);

    ArrayList<Object> decode(double[][][] source);

    boolean canDecode();

    JDialog getDialog();

    @SuppressWarnings("unchecked")
    static <T> T[] deepCopyOf(T[] array) {

        if (0 >= array.length) return array;

        return (T[]) deepCopyOf(array, Array.newInstance(array[0].getClass(), array.length), 0);
    }

    static Object deepCopyOf(Object array, Object copiedArray, int index) {
        if (index >= Array.getLength(array)) return copiedArray;
        Object element = Array.get(array, index);
        if (element.getClass().isArray()) {
            Array.set(copiedArray, index, deepCopyOf(element, Array.newInstance(element.getClass().getComponentType(), Array.getLength(element)), 0));
        } else {
            Array.set(copiedArray, index, element);
        }
        return deepCopyOf(array, copiedArray, ++index);
    }
}
