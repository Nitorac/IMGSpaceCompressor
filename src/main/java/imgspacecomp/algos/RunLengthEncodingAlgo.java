package imgspacecomp.algos;

import imgspacecomp.forms.RLESettings;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;

public class RunLengthEncodingAlgo implements BaseAlgo {

    public double tolerance = 6.5; //Pourcent
    public Direction direction = Direction.LIGNE;
    private RLESettings dialog;

    public RunLengthEncodingAlgo() {
        dialog = new RLESettings(this);
    }

    public String getName() {
        return "RLE (Run-Length)";
    }

    public String[] getAvantages() {
        return new String[0];
    }

    public String[] getInconvenients() {
        return new String[0];
    }

    @Override
    public String getErrorMessage(Exception e) {
        return null;
    }

    public ArrayList<Object> encode(double[][][] source) {
        double[][][] res = BaseAlgo.deepCopyOf(source);
        ArrayList<Object> arr = direction.getEncodedMatrix(res, tolerance);
        return new ArrayList<>(Arrays.asList((int) arr.get(0), (double[][][]) arr.get(1)));
    }

    public JDialog getDialog() {
        return dialog;
    }

    public ArrayList<Object> decode(double[][][] source) {
        return new ArrayList<>(Arrays.asList(1, source));
    }

    public boolean canDecode() {
        return false;
    }

    public enum Direction {
        LIGNE("LIGNE"),
        COLONNE("COLONNE");

        String id;

        Direction(String id) {
            this.id = id;
        }

        public String getID() {
            return id;
        }

        public ArrayList<Object> getEncodedMatrix(double[][][] src, double tolerance) {
            int finalSize = 0;
            int totalIterations = 0;
            int maxOctSize = 1;
            switch (id) {
                case "LIGNE":
                    for (int i = 0; i < src.length; i++) {
                        int currentRangeStart = 0;
                        double[] currentPixel = src[i][currentRangeStart];
                        int iteration = 0;
                        for (int j = 0; j < src[0].length; j++) {
                            if (diffCoul(i, currentRangeStart, i, j, src, tolerance / 100)) {
                                src[i][j] = currentPixel;
                            } else {
                                currentPixel = src[i][j];
                                currentRangeStart = j;
                                //    Nb de repetitions + taille d'un pixel (3 octets)
                                int max = 1 + (j - currentRangeStart) / 255;
                                if (max > maxOctSize) {
                                    maxOctSize = max;
                                }
                                iteration += 1;
                            }
                        }
                        totalIterations += iteration;
                    }
                    finalSize = totalIterations * (maxOctSize + src[0][0].length);
                    break;
                case "COLONNE":
                    for (int j = 0; j < src[0].length; j++) {
                        int currentRangeStart = 0;
                        double[] currentPixel = src[currentRangeStart][j];
                        int iteration = 0;
                        for (int i = 0; i < src.length; i++) {
                            if (diffCoul(currentRangeStart, j, i, j, src, tolerance / 100)) {
                                src[i][j] = currentPixel;
                            } else {
                                currentPixel = src[i][j];
                                currentRangeStart = i;
                                //    Nb de repetitions + taille d'un pixel (3 octets)
                                int max = 1 + (i - currentRangeStart) / 255;
                                if (max > maxOctSize) {
                                    maxOctSize = max;
                                }
                                iteration += 1;
                            }
                        }
                        totalIterations += iteration;
                    }
                    finalSize = totalIterations * (maxOctSize + src[0][0].length);
                default:
            }

            return new ArrayList<>(Arrays.asList(finalSize, src));
        }


        // Différence des 3 couleurs des pixels sachant que les 3 couleurs doivent respecter la tolérance
        private boolean diffCoul(int i, int j, int x, int y, double[][][] res, double tol) {
            double[] ref = res[i][j];
            double[] pix = res[x][y];
            return Math.abs(ref[0] - pix[0]) <= tol && Math.abs(ref[1] - pix[1]) <= tol && Math.abs(ref[2] - pix[2]) <= tol;
        }
    }
}