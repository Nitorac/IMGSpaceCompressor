package imgspacecomp.algos;

import imgspacecomp.Utils;
import imgspacecomp.forms.HaarSettings;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;

public class HaarOndelettesAlgo implements BaseAlgo {

    public double epsilon = 10.0D; //Percent
    public int iterations = 1;
    private HaarSettings dialog;

    public HaarOndelettesAlgo() {
        dialog = new HaarSettings(this);
    }

    public String getName() {
        return "Ondelettes Haar";
    }

    public String[] getAvantages() {
        return new String[0];
    }

    public String[] getInconvenients() {
        return new String[0];
    }

    @Override
    public String getErrorMessage(Exception e) {
        if (e instanceof IllegalArgumentException) {
            return "L'image doit être carrée de taille une puissance de 2 !";
        }
        return e.getMessage();
    }

    public ArrayList<Object> encode(double[][][] source) throws IllegalArgumentException {
        if (source.length != source[0].length || !Utils.powerOf2(source.length) || !Utils.powerOf2(source[0].length)) {
            Utils.log("Erreur: Tailles " + source.length + " X " + source[0].length);
            throw new IllegalArgumentException();
        }
        Utils.log("dim source = " + source.length + " X " + source[0].length);

        double[][][] res = Utils.deepCopy(source);
        int sizeToApply = res.length;

        // Décomposition ...
        for (int i = 0; i < iterations; i++) {
            res = mergeImg(res, encodeImg(subImg(res, sizeToApply)));
            sizeToApply /= 2;
        }


        //Reconstruction ...
        for (int i = 0; i < iterations; i++) {
            sizeToApply *= 2;
            res = mergeImg(res, decodeImg(subImg(res, sizeToApply)));
        }

        Utils.log("Haar fini !");
        Utils.log("Calcul des compressions");
        return new ArrayList<>(Arrays.asList(calculComp(res), res));
    }

    public double[][][] encodeImg(double[][][] source) throws IllegalArgumentException {
        double[][][] res = Utils.deepCopy(source);

        for (int k = 0; k < source[0][0].length; k++) {
            for (int i = 0; i < res.length; i = i + 2) {
                for (int j = 0; j < res[i].length; j++) {
                    double newSpi = (source[i][j][k] + source[i + 1][j][k]) / 2;
                    double newDpi = (source[i][j][k] - source[i + 1][j][k]) / 2;

                    res[i / 2][j][k] = newSpi;
                    res[res.length / 2 + i / 2][j][k] = newDpi;
                }
            }

            double[][][] newSource = Utils.deepCopy(res);

            // Colonnes
            for (int j = 0; j < newSource.length; j = j + 2) {
                for (int i = 0; i < newSource[j].length; i++) {
                    double newSpi = (newSource[i][j][k] + newSource[i][j + 1][k]) / 2;
                    double newDpi = (newSource[i][j][k] - newSource[i][j + 1][k]) / 2;

                    res[i][j / 2][k] = newSpi;
                    res[i][res.length / 2 + j / 2][k] = newDpi;
                }
            }
        }
        return res;
    }

    public double[][][] decodeImg(double[][][] source) {
        double[][][] res = Utils.deepCopy(source);

        for (int k = 0; k < source[0][0].length; k++) {
            // Colonnes
            for (int j = 0; j < res.length / 2; j++) {
                for (int i = 0; i < res[j].length; i++) {
                    double newSpi = source[i][j][k];
                    double newDpi = source[i][res.length / 2 + j][k];
                    if (Math.abs(newDpi) < epsilon / 100.0) {
                        newDpi = 0;
                    }
                    res[i][2 * j][k] = newSpi + newDpi;
                    res[i][2 * j + 1][k] = newSpi - newDpi;
                }
            }

            double[][][] newSource = Utils.deepCopy(res);

            for (int i = 0; i < newSource.length / 2; i++) {
                for (int j = 0; j < newSource[i].length; j++) {
                    double newSpi = newSource[i][j][k];
                    double newDpi = newSource[res.length / 2 + i][j][k];
                    if (Math.abs(newDpi) < epsilon / 100.0) {
                        newDpi = 0;
                    }
                    res[2 * i][j][k] = newSpi + newDpi;
                    res[2 * i + 1][j][k] = newSpi - newDpi;
                }
            }

        }
        return res;
    }

    // Starting from top left corner
    public double[][][] subImg(double[][][] in, int size) {
        double[][][] res = new double[size][size][in[0][0].length];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                for (int k = 0; k < in[0][0].length; k++) {
                    res[i][j][k] = in[i][j][k];
                }
            }
        }
        return res;
    }

    public double[][][] mergeImg(double[][][] root, double[][][] part) {
        double[][][] res = Utils.deepCopy(root);
        for (int i = 0; i < part.length; i++) {
            for (int j = 0; j < part[0].length; j++) {
                System.arraycopy(part[i][j], 0, res[i][j], 0, part[0][0].length);
            }
        }
        return res;
    }

    public double[][] reshape3To2(double[][][] src, int k) {
        double[][] res = new double[src.length][src[0].length];
        for (int i = 0; i < src.length; i++) {
            for (int j = 0; j < src.length; j++) {
                res[i][j] = src[i][j][k];
            }
        }
        return res;
    }

    public int calculComp(double[][][] res) {
        int totalIterations = 0;
        int maxOctSize = 1;
        for (int i = 0; i < res.length; i++) {
            int currentRangeStart = 0;
            int iteration = 0;
            for (int j = 0; j < res[0].length; j++) {
                if (!diffCoul(i, currentRangeStart, i, j, res, 0.00001)) {
                    currentRangeStart = j;
                    //    Nb de repetitions + taille d'un pixel (3 octets)
                    int max = 1 + (j - currentRangeStart) / 255;
                    if (max > maxOctSize) {
                        maxOctSize = max;
                    }
                    iteration += 1;
                }
            }
            totalIterations += iteration;
        }
        return totalIterations * (maxOctSize + res[0][0].length);
    }

    private boolean diffCoul(int i, int j, int x, int y, double[][][] res, double tol) {
        double[] ref = res[i][j];
        double[] pix = res[x][y];
        return Math.abs(ref[0] - pix[0]) <= tol && Math.abs(ref[1] - pix[1]) <= tol && Math.abs(ref[2] - pix[2]) <= tol;
    }

    public ArrayList<Object> decode(double[][][] source) throws IllegalArgumentException {
        return new ArrayList<>(Arrays.asList(1, source));
    }

    @Override
    public boolean canDecode() {
        return true;
    }

    public JDialog getDialog() {
        return dialog;
    }
}
