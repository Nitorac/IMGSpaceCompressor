package imgspacecomp;

import aliceinnets.python.jyplot.JyPlot3;
import imgspacecomp.algos.BaseAlgo;
import imgspacecomp.core.ImageEncoder;
import imgspacecomp.forms.MainForm;

import javax.swing.*;
import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class Main {

    public static MainForm mF;
    public static ImageEncoder encoder;
    public static JyPlot3 plt;
    public static boolean DEBUG = true;

    public static void main(String[] args) {
        JFrame frame = new JFrame("Compresseur d'image");
        mF = new MainForm();
        encoder = new ImageEncoder();
        JyPlot3.setPythonPath((args.length > 0) ? args[0] : "C:/Python37/python.exe");
        plt = new JyPlot3();
        frame.setContentPane(mF.mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        initWindow();
        Utils.centerWindow(frame);
        frame.setVisible(true);
    }

    public static void drawImage(BaseAlgo alg, double ratio, int finalOctets, int srcOctets, double[][][] compressed, double[][][] src) {
        NumberFormat nbFormat = NumberFormat.getInstance(Locale.FRENCH);
        plt = new JyPlot3();
        plt.write("f,axarr = plt.subplots(1, 2, sharey=True, sharex=True)");
        plt.write("plt.sca(axarr[0])");
        plt.write("axarr[0].set_title('Image Originale')");
        plt.write("axarr[0].set_xlabel('Taille de l\\'image originale : " + nbFormat.format(srcOctets) + " octets')");
        plt.imshow(src, null, null, null, "nearest");
        plt.write("plt.sca(axarr[1])");
        plt.write("axarr[1].set_title('" + alg.getName() + "')");
        plt.write("axarr[1].set_xlabel('Taille de l\\'image après compression (estimation) : " + nbFormat.format(finalOctets) + " octets')");
        plt.imshow(compressed, null, null, null, "nearest");
        plt.write("mng = plt.get_current_fig_manager()");
        plt.write("mng.window.state('zoomed')");
        nbFormat.setMaximumFractionDigits(3);
        nbFormat.setMinimumFractionDigits(3);
        plt.write("f.suptitle('\\nFacteur de compression : " + nbFormat.format(ratio) + "', fontsize=12)");
        plt.show();
        plt.exec();
    }

    public static void initWindow() {
        mF.algoChoose.addActionListener(event -> {
            BaseAlgo alg = Utils.findAlgoFromString((String) mF.algoChoose.getSelectedItem());
            if (alg == null) {
                System.out.println("Problème de choix d'algo :(");
                return;
            }
        });

        mF.findBtn.addActionListener(e -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("/"));
            chooser.changeToParentDirectory();
            int response = chooser.showOpenDialog(null);
            if (response == JFileChooser.APPROVE_OPTION) {
                mF.inputField.setText(chooser.getSelectedFile().getAbsolutePath());
            } else {
                System.out.println("Choix de dossier annulé !");
            }
        });

        Arrays.asList(Utils.ALGOS_LIST).forEach(baseAlgo -> mF.algoChoose.addItem(baseAlgo.getName()));
        mF.algoChoose.setSelectedIndex(0);

        mF.settingsBtn.addActionListener(event -> {
            BaseAlgo alg = Utils.findAlgoFromString((String) mF.algoChoose.getSelectedItem());
            if (alg == null) {
                System.out.println("Problème de choix d'algo :(");
                return;
            }

            JDialog dialog = alg.getDialog();
            if (dialog == null) {
                System.out.println("Impossible de trouver des paramètres");
                return;
            }
            dialog.pack();
            Utils.centerWindow(dialog);
            dialog.setVisible(true);
        });

        mF.previewBtn.addActionListener(event -> {
            String path = mF.inputField.getText().trim().replaceAll("\"", "");
            File inputFile = null;
            if (!path.isEmpty() && (inputFile = new File(path)).exists()) {
                BaseAlgo baseAlgo = Utils.findAlgoFromString((String) mF.algoChoose.getSelectedItem());
                if (baseAlgo == null) {
                    System.out.println("Problème de choix d'algo :(");
                    return;
                }

                try {
                    double[][][] srcInt = encoder.readImageToIntArray(path);
                    ArrayList<Object> returned = baseAlgo.encode(srcInt);
                    double[][][] res = (double[][][]) returned.get(1);
                    int srcOcts = res.length * res[0].length * res[0][0].length;
                    File destFile = new File(inputFile.getParentFile().getAbsolutePath(), Utils.getNameWOExt(inputFile.getName()) + "-compressed." + Utils.getExt(inputFile.getName()));
                    encoder.saveImageToDisk(encoder.getBufImgFromIntArray(res), destFile, Utils.getExt(inputFile.getName()));
                    drawImage(baseAlgo, (double) srcOcts / (double) ((int) returned.get(0)), (int) returned.get(0), srcOcts, res, srcInt);
                } catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(mF.mainPanel,
                        baseAlgo.getErrorMessage(e),
                        "Erreur",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }
}
