package imgspacecomp.forms;


import javax.swing.*;

public class MainForm {
    public JLabel inputLabel;
    public JTextField inputField;
    public JButton findBtn;
    public JPanel mainPanel;
    public JLabel algoChooseLbl;
    public JComboBox<String> algoChoose;
    public JButton settingsBtn;
    public JButton previewBtn;
}
