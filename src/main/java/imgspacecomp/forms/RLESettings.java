package imgspacecomp.forms;

import imgspacecomp.algos.RunLengthEncodingAlgo;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;

public class RLESettings extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel contentPanel;
    private JLabel toleranceLbl;
    private JTextField toleranceTextField;
    private JLabel directionLbl;
    private JComboBox<String> directionComboBox;
    private RunLengthEncodingAlgo parent;

    public RLESettings(RunLengthEncodingAlgo parent) {
        setContentPane(contentPane);
        setModal(true);
        setTitle("Paramètres du RLE");
        getRootPane().setDefaultButton(buttonOK);
        this.parent = parent;

        Arrays.stream(RunLengthEncodingAlgo.Direction.values()).forEach(dir -> directionComboBox.addItem(dir.getID()));

        toleranceTextField.setText(String.valueOf(parent.tolerance));
        directionComboBox.setSelectedItem(parent.direction.getID());

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        String tol = toleranceTextField.getText().trim();
        if (toleranceTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                "Vous devez compléter tous les paramètres",
                "Erreur",
                JOptionPane.ERROR_MESSAGE);
            return;
        }
        parent.tolerance = Double.parseDouble(tol);
        parent.direction = RunLengthEncodingAlgo.Direction.valueOf((String) directionComboBox.getSelectedItem());
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}
