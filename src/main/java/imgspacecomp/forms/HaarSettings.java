package imgspacecomp.forms;

import imgspacecomp.algos.HaarOndelettesAlgo;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class HaarSettings extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField epsilonTextField;
    private JLabel epsilon;
    private JLabel iterationLabel;
    private JTextField iterationTextField;
    private HaarOndelettesAlgo parent;

    public HaarSettings(HaarOndelettesAlgo parent) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Paramètres des ondelettes de Haar");
        this.parent = parent;
        buttonOK.addActionListener(e -> onOK());
        buttonCancel.addActionListener(e -> onCancel());

        epsilonTextField.setText(String.valueOf(parent.epsilon));
        iterationTextField.setText(String.valueOf(parent.iterations));

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public int getEpsilon() {
        return (int) Double.parseDouble(epsilonTextField.getText());
    }

    public int getIterations() {
        return Integer.parseInt(iterationTextField.getText());
    }

    private void onOK() {
        String tol = epsilonTextField.getText().trim();
        if (epsilonTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                "Vous devez compléter tous les paramètres",
                "Erreur",
                JOptionPane.ERROR_MESSAGE);
            return;
        }
        parent.epsilon = Double.parseDouble(tol);
        String it = iterationTextField.getText().trim();
        if (iterationTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this,
                "Vous devez compléter tous les paramètres",
                "Erreur",
                JOptionPane.ERROR_MESSAGE);
            return;
        }
        parent.iterations = Integer.parseInt(it);
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}
