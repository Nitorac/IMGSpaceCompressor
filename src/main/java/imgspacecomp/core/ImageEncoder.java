package imgspacecomp.core;

import imgspacecomp.Utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageEncoder {

    public ImageEncoder() {

    }

    public boolean writeImageFromIntArray(double[][][] pixels, String path) throws IOException {
        return ImageIO.write(getBufImgFromIntArray(pixels), "tif", new File(path));
    }

    public double[][][] readImageToIntArray(String path) throws IOException {
        BufferedImage image = ImageIO.read(new File(path));
        double ret[][][] = new double[image.getHeight()][image.getWidth()][3];
        int oneD[] = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
        for (int i = 0; i < image.getHeight(); i++)
            for (int j = 0; j < image.getWidth(); j++)
                for (int k = 2; k >= 0; k--) {
                    ret[i][j][k] = (double) (oneD[i * image.getWidth() + j] & 0x000000FF) / (double) 255;
                    oneD[i * image.getWidth() + j] = oneD[i * image.getWidth() + j] >> 8;
                }
        return ret;
    }

    /*public int[][][] readImageToIntArray(String path) throws IOException {
        BufferedImage img = ImageIO.read(new File(path));
        int w = img.getWidth();
        int h = img.getHeight();
        int[][][] pixels = new int[h][w][3];
        for(int i = 0; i < w; i++){
            for(int j = 0; j < h; j++){
                Color c = new Color(img.getRGB(i, j));
                pixels[j][i] = new int[]{c.getRed(), c.getGreen(), c.getBlue()};
            }
        }

        return pixels;
    }*/

    public void saveImageToDisk(BufferedImage img, File dest, String format) {
        try {
            if (dest.exists()) {
                dest.delete();
            }
            ImageIO.write(img, format, dest);
            Utils.log("Image enregistree : " + dest.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BufferedImage getBufImgFromIntArray(double[][][] pixels) {
        int w = pixels[0].length;
        int h = pixels.length;
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                double[] pix = pixels[j][i];
                for (int k = 0; k < pix.length; k++) {
                    pix[k] = (pix[k] < 0) ? 0 : ((pix[k] > 1) ? 1 : pix[k]);
                }
                Color c = new Color((float) pix[0], (float) pix[1], (float) pix[2]);
                image.setRGB(i, j, c.getRGB());
            }
        }

        return image;
    }
}
