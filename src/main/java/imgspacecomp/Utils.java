package imgspacecomp;

import imgspacecomp.algos.BaseAlgo;
import imgspacecomp.algos.HaarOndelettesAlgo;
import imgspacecomp.algos.RunLengthEncodingAlgo;

import java.awt.*;
import java.util.Arrays;

public class Utils {
    public static final BaseAlgo[] ALGOS_LIST = {new RunLengthEncodingAlgo(), new HaarOndelettesAlgo()};
    public static final String[] ENCODE_BOX = {"Compresser", "Décompresser"};

    public static BaseAlgo findAlgoFromString(String str) {
        return Arrays.stream(Utils.ALGOS_LIST).filter(alg -> alg.getName().equalsIgnoreCase(str)).findFirst().orElse(null);
    }

    public static void log(String str) {
        if (Main.DEBUG) {
            System.out.println(str);
        }
    }

    public static void centerWindow(Window win) {
        win.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width) / 2 - win.getWidth() / 2, (Toolkit.getDefaultToolkit().getScreenSize().height) / 2 - win.getHeight() / 2);
    }

    public static boolean powerOf2(int nb) {
        return nb > 0 && ((nb & (nb - 1)) == 0);
    }

    public static int pow(int x, int p) {
        int temp;
        if (p == 0) return 1;
        else if ((p % 2) == 0) {
            temp = pow(x, p / 2);
            return temp * temp;
        } else {
            temp = pow(x, (p - 1) / 2);
            return x * temp * temp;
        }
    }

    public static String getNameWOExt(String str) {
        if (str == null) return null;
        int pos = str.lastIndexOf(".");
        if (pos == -1) return str;
        return str.substring(0, pos);
    }

    public static String getExt(String str) {
        if (str == null) return null;
        int pos = str.lastIndexOf(".");
        if (pos == -1 || pos + 1 >= str.length()) return str;
        return str.substring(pos + 1, str.length());
    }

    public static double[] rgb2yuv(short R, short G, short B) {
        double Y = R * .299000 + G * .587000 + B * .114000;
        double U = R * -.168736 + G * -.331264 + B * .500000 + 128;
        double V = R * .500000 + G * -.418688 + B * -.081312 + 128;
        return new double[]{Y, U, V};
    }

    public static double[] yuv2rgb0to1(double Y, double U, double V) {
        double R = (Y + 1.4075 * (V - 128));
        double G = (Y - 0.3455 * (U - 128) - (0.7169 * (V - 128)));
        double B = (Y + 1.7790 * (U - 128));
        return new double[]{R, G, B};
    }

    public static double[][][] deepCopy(double[][][] in) {
        double[][][] res = new double[in.length][in[0].length][in[0][0].length];
        for (int i = 0; i < in.length; i++) {
            for (int j = 0; j < in[0].length; j++) {
                for (int k = 0; k < in[0][0].length; k++) {
                    res[i][j][k] = in[i][j][k];
                }
            }
        }
        return res;
    }
}
